export class UrlConstant {
    public static ApiLocation = 'http://localhost:5000';
    public static API_ENDPOINT = UrlConstant.ApiLocation + '/api/';


    public static API_Add = '/assets/json/Add.json/';
    public static API_Multiply = '/assets/json/Multiply.json/';
    public static API_Numbers = '/assets/json/Numbers.json/';
}
