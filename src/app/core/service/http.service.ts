import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { Add } from 'src/app/data/model/add.model';
import { Multiply } from 'src/app/data/model/multiply.model';
import { Number } from 'src/app/data/model/number.model';
import { UrlConstant } from '../../constants/urlConstant';

@Injectable({
  providedIn: 'root',
})
export class HttpService {
  constructor(private http: HttpClient) {}

  getMultiplyJson():Observable<Multiply> {
    return this.http.get<Multiply>(UrlConstant.API_Multiply)
  }

  getAddJson(): Observable<Add>  {
    return this.http.get<Add>(UrlConstant.API_Add)
  }

  getNumbersJson():Observable<Number[]> {
    return this.http.get<Number[]>(UrlConstant.API_Numbers)
  }


}
