/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { HttpService } from './http.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { UrlConstant } from 'src/app/constants/urlConstant';
import { Action } from 'src/app/data/enum/action.enum';
import { Number } from 'src/app/data/model/number.model';

describe('Service: Http', () => {
  let httpMock: HttpTestingController;
  let httpService: HttpService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpService],
    });

    httpService = TestBed.get(HttpService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should ...', inject([HttpService], (service: HttpService) => {
    expect(service).toBeTruthy();
  }));

  it('getMultiplyJson() should  GET MultiplyJson data in assets/json',async () => {
    const result = { value: 10 };

    httpService.getMultiplyJson().subscribe((res) => {
      expect(res).toEqual(result);
    });

    const req = httpMock.expectOne(UrlConstant.API_Multiply);
    expect(req.request.method).toEqual('GET');
    req.flush(result);

    httpMock.verify();
  });

  it('getAddJson() should  GET AddJson file in assets/json',async () => {
    const result = { value: 10 };

    httpService.getAddJson().subscribe((res) => {
      expect(res).toEqual(result);
    });

    const req = httpMock.expectOne(UrlConstant.API_Add);
    expect(req.request.method).toEqual('GET');
    req.flush(result);

    httpMock.verify();
  });

  it('getAddJson() should  GET AddJson file in assets/json', () => {
    const result = { value: 5 };

    httpService.getAddJson().subscribe((res) => {
      expect(res).toEqual(result);
    });

    const req = httpMock.expectOne(UrlConstant.API_Add);
    expect(req.request.method).toEqual('GET');
    req.flush(result);

    httpMock.verify();
  });

  it('getNumbersJson() should  GET NumbersJson file in assets/json',async () => {

    const result: Number[] = [
      { value: 1, action: Action['add'], result: 0 },
      { value: 2, action: Action['multiply'], result: 0 },
      { value: 3, action: Action['add'], result: 0 },
      { value: 4, action: Action['add'], result: 0 },
      { value: 5, action: Action['multiply'], result: 0 },
      { value: 6, action: Action['multiply'], result: 0 },
    ];

    httpService.getNumbersJson().subscribe((res) => {
      expect(res).toEqual(result);
    });

    const req = httpMock.expectOne(UrlConstant.API_Numbers);
    expect(req.request.method).toEqual('GET');
    req.flush(result);

    httpMock.verify();
  });
});
