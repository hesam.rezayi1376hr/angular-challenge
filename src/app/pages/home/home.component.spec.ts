import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatSnackBar, MatSnackBarModule } from '@angular/material/snack-bar';
import { HomeComponent } from './home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { HttpService } from 'src/app/core/service/http.service';
import { NgxSpinnerService,NgxSpinnerModule } from 'ngx-spinner';
import { Action } from 'src/app/data/enum/action.enum';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Number } from 'src/app/data/model/number.model';
import { of } from 'rxjs';
import { Add } from 'src/app/data/model/add.model';
import { Multiply } from 'src/app/data/model/multiply.model';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let httpService: HttpService;
  let ngxSpinnerService: NgxSpinnerService;
  let fixture: ComponentFixture<HomeComponent>;
  const numberResult: Number[] = [
    { value: 1, action: Action.Add, result: 0 },
    { value: 2, action: Action.Multiply, result: 0 },
    { value: 3, action: Action.Add, result: 0 },
    { value: 4, action: Action.Add, result: 0 },
    { value: 5, action: Action.Multiply, result: 0 },
    { value: 6, action: Action.Multiply, result: 0 },
  ];
  const addResult = { value: 5 };
  const multiplyResult = { value: 10 };
  let snackBar: MatSnackBar;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      imports: [HttpClientTestingModule,MatSnackBarModule,NgxSpinnerModule,BrowserAnimationsModule], 
      providers: [HttpService,MatSnackBar,NgxSpinnerService]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    httpService= TestBed.get(HttpService);
    snackBar= TestBed.get(MatSnackBar);
    ngxSpinnerService= TestBed.get(NgxSpinnerService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(()=>{
    component.numbers = [];
    component.add = new Add();
    component.multiply= new Multiply();
  })

  it('should create HomeComponent', () => {
    expect(component).toBeTruthy();
  });  
  
  it('HttpService should be created', () => {
    expect(httpService).toBeTruthy();
  });
  
  it('MissingData Should appear when AddJson not loaded',async () => {
    component.numbers  = [{ value: 1, action: Action.Add, result: 0 }];
    component.addMissingData = true;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('span').textContent).toContain('MISSING DATA');
  }); 

  it('MissingData Should appear when MultiplyJson not loaded',async () => {
    component.numbers  = [{ value: 1, action: Action.Multiply, result: 0 }];
    component.multiplyMissingData = true;
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('span').textContent).toContain('MISSING DATA');
  });  

  it('get data functions work ',async () => {
    let spyGetNumbersJson = spyOn(httpService, 'getNumbersJson').and.returnValue(of(numberResult));
    let spyGetMultiplyJson = spyOn(httpService, 'getMultiplyJson').and.returnValue(of(multiplyResult));
    let spyGetAddJson = spyOn(httpService, 'getAddJson').and.returnValue(of(addResult));
    component.init();
    fixture.whenStable().then(() => {
      expect(spyGetNumbersJson).toHaveBeenCalled();
      expect(spyGetMultiplyJson).toHaveBeenCalled();
      expect(spyGetAddJson).toHaveBeenCalled();

      expect(component.numbers).toEqual(numberResult);
      expect(component.add).toEqual(addResult);
      expect(component.multiply).toEqual(multiplyResult);
    });
  });
});
