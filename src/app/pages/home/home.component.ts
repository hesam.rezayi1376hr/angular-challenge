import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { switchMap } from 'rxjs/operators';
import { HttpService } from 'src/app/core/service/http.service';
import { Add } from 'src/app/data/model/add.model';
import { Multiply } from 'src/app/data/model/multiply.model';
import { Number } from 'src/app/data/model/number.model';
import { Action } from '../../data/enum/action.enum';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  Action = Action;
  addMissingData: boolean = false;
  multiplyMissingData: boolean = false;
  numbers: Number[] = [];
  add: Add = new Add();
  multiply: Multiply = new Multiply();

  constructor(
    private httpService: HttpService,
    private matSnackBar: MatSnackBar,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    this.spinner.show();
    setTimeout(() => {
      this.init();
    }, 1000);
  }

  init = () => {
    this.getAddJson();
    this.getMultiplyJson();
    this.getNumberJson();
  };

  getNumberJson = () => {
    this.httpService
      .getNumbersJson()
      .pipe(
        switchMap((val: Number[]) => {
           return val;
        })
      )
      .subscribe(
        (res: Number) => {
          res.result =
            res.action == Action.Add
              ? res.value + this.add.value
              : res.value * this.multiply.value;
          this.numbers.push(res);
          this.spinner.hide();
        },
        (error) => {
          this.ShowSnackBar('Server Error');
          this.spinner.hide();
        },
      );
  };

  getMultiplyJson =  () => {
    this.httpService.getMultiplyJson().subscribe(
      (response: Multiply) => {
        this.multiply = response;
      },
      (error) => {
        this.multiplyMissingData = true;
      }
    );
  };

  getAddJson = () => {
    this.httpService.getAddJson().subscribe(
      (response: Add) => {
        this.add = response;
      },
      (error) => {
        this.addMissingData = true;
      }
    );
  };

  ShowSnackBar(message: string) {
    this.matSnackBar.open(message, 'Remove', {
      duration: 3000,
    });
  }
}
