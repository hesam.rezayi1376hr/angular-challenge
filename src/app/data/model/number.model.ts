import { Action } from "../enum/action.enum"

export class Number {
    constructor(value : number , action : string){
        this.value = value;
        this.action = Action[action]
    }
    value : number
    action : Action
    result: number
}
